package com.rabbitmq.demo;

import static org.junit.Assert.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.Rule;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.amqp.rabbit.junit.BrokerRunning;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.ApplicationContext;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;

import com.rabbitmq.demo.producer.RabbitMQProducer;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = { RabbitMqDemoDLApplication.class })
@ActiveProfiles(profiles = "test")
class RabbitMqDemoApplicationTests {

	@Rule
	public BrokerRunning brokerRunning = BrokerRunning.isRunning();

	@Autowired
	private ApplicationContext applicationContext;

	@Autowired
	private RabbitTemplate rabbitTemplate;

	@Test
	public void test() {
		assertNotNull(applicationContext);
	}

	@Test
	public void testProducer() {

		RabbitMQProducer rbp = new RabbitMQProducer();
		assertEquals(rbp.producer("Maya", "123", 5000), "Message Sent to Server Successfully");
	}

}
