package com.rabbitmq.demo.producer;

import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.rabbitmq.demo.dto.Employee;



@RestController
@RequestMapping("/trial/")
public class RabbitMQProducer {

    @Autowired
    private RabbitTemplate template;

    @GetMapping("/producer")
    public String producer(@RequestParam("empName") String empName,@RequestParam("empId") String empId,@RequestParam("salary") int salary) {
      
    	Employee emp=new Employee();
		emp.setEmpId(empId);
		emp.setEmpName(empName);
		emp.setSalary(salary);
    	
    	template.convertAndSend("normalExchange", "normal", emp);
    
    	
		return "Message Sent to Server Successfully";
    }
}
